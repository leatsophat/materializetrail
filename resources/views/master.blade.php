<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8">
    {{-- header style --}}
    @include('front-end.layout.header')
    {{-- End header style --}}
</head>

<body class="has-fixed-sidenav">

    {{-- NavBar --}}
    @include('front-end.layout.navbar')
    {{-- End NavBar --}}


    <main class="pdd-10px">
        {{-- Full Content--}}
        @yield('content')
        {{-- End Full Content--}}
    </main>


    {{-- Footer Menu --}}
    @include('front-end.layout.footermenu')
    {{-- End Footer Menu --}}


    {{-- Footer --}}
    @include('front-end.layout.footer')
    {{-- End Footer --}}


    {{-- Footer Script --}}
    @yield('AddMoreScript')
    {{-- End Footer Script --}}

</body>
</html>
