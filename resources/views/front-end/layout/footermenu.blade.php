<footer class="page-footer pdd-0">
    <div class="footer-copyright">
        <div class="w-100 row">
            <div class="col s12 m9 l8">
                <div class="left black-text pdd-left-20px mobile-center"><b> Copyright ©2020-<?php echo date("Y"); ?> <a class="blue-text text-bold" href="#!">TURBOTECH CO., LTD.</b></a> All rights reserved. </div>
            </div>
            <div class="col s12 m3 l4">
                <div class="right black-text text-right pdd-right-20px  mobile-center"><b>Version 2.0.0</b></div>
            </div>
        </div>
    </div>
</footer>
