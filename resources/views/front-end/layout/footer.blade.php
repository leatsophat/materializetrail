<script src="{{ asset('https://code.jquery.com/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('back-end/js/moment.min.js') }}"></script>


{{-- <script src="{{ asset('https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.js') }}"></script> --}}

<script src="{{ asset('front-end/js/materialize.min.js') }}"></script>
<script src="{{ asset('front-end/js/materialize.js') }}"></script>

<script src="{{ asset('front-end/js/validation.js') }}"></script>
<script src="{{ asset('back-end/js/init.js') }}"></script>

<script src="{{ asset('back-end/js/dashboard.min.js') }}"></script>
{{-- <script src="//cdn.shopify.com/s/files/1/1775/8583/t/1/assets/admin-materialize.min.js?v=2611245393728459541"></script> --}}