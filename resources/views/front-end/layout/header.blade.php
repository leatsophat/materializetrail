<head>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content="{{ '' }}" />
    <meta name="robots" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
    <meta property="og:locale" content="{{ str_replace('_', '-', app()->getLocale()) }}" />
    <meta property="og:image" content="{{ asset('back-end/media/logo/tt-logo.png') }}">
    <meta property="og:image:type" content="{{ asset('back-end/media/logo/tt-logo.png') }}">
    <meta property="og:type" content="{{''}}" />
    <meta property="og:title" content="{{ $title }}" />
    <meta property="og:description" content="{{''}}" />
    <link rel="shortcut icon" href="{{ asset('back-end/media/logo/tt-logo.png') }}" />
    <meta property="og:title" content="{{ $title }}" />
    <title>{{ $title }}</title>

    <link rel="stylesheet" href="{{ asset('https://fonts.googleapis.com/icon?family=Material+Icons') }}">

    {{-- web UI --}}
    {{-- <link rel="stylesheet" href="{{ asset('front-end/plugins/materialize-css/dist/css/materialize.min.css') }}">
    <link rel="stylesheet" href="{{ asset('front-end/plugins/materialize-css/dist/css/materialize.css') }}"> --}}

    <link rel="stylesheet" href="{{ asset('back-end/css/admin.min.css') }}">
    <link rel="stylesheet" href="{{ asset('back-end/css/adminstyle.css') }}">
    
    {{-- front-end --}}
    <link rel="stylesheet" href="{{ asset('https://sb-library.netlify.app/css/style2.css') }}">
    <link rel="stylesheet" href="{{ asset('back-end/css/media.css') }}">



    {{-- custome style --}}
    <link rel="stylesheet" href="{{ asset('front-end/css/font.css') }}">

</head>
