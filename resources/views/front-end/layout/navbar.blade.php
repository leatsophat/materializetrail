<header>
    <div class="navbar-fixed">
        <nav class="navbar blue">
            <div class="nav-wrapper">
                {{-- <a href="#!" class="brand-logo white-text">Home</a> --}}
                <ul id="nav-mobile" class="right">
                    <li><a href="#!" data-target="chat-dropdown" class="dropdown-trigger waves-effect white-text"><i class="material-icons ">power_settings_new</i></a></li>
                </ul>
                <a href="#" data-target="sidenav-left" class="sidenav-trigger left"><i class="material-icons white-text">menu</i></a>
            </div>
        </nav>
    </div>

    <ul id="sidenav-left" class="sidenav sidenav-fixed white">
        <li>
            <a href="/" class="logo-container white center">
                <img src="https://www.turbotech.com/storages/assets/img/system/turbotech.png" alt="turbotech" width="40%">
            </a> 
        </li>
        <li class="no-padding blue">
            <ul class="collapsible collapsible-accordion">
                <li class="bold waves-effect waves-red"><a class="collapsible-header white-text bold-text">Dashboard</a>
            </ul>
        </li>
        <li class="no-padding blue">
            <ul class="collapsible collapsible-accordion">
                <li class="bold waves-effect waves-red active"><a class="collapsible-header white-text bold-text">E-Request<i class="material-icons chevron white-text">chevron_left</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="/permission" class="waves-effect waves-red white-text active bold-text">Permission<i class="material-icons white-text">people</i></a></li>
                            <li><a href="#" class="waves-effect waves-red white-text bold-text">Mission<i class="material-icons white-text">group_add</i></a></li>
                            <li><a href="#" class="waves-effect waves-red white-text bold-text">Purchase Requesition<i class="material-icons white-text">add_shopping_cart</i></a></li>
                            <li><a href="#" class="waves-effect waves-red white-text bold-text">Installation<i class="material-icons white-text">transfer_within_a_station</i></a></li>
                            <li><a href="#" class="waves-effect waves-red white-text bold-text">Overtime<i class="material-icons white-text">timer</i></a></li>
                        </ul>
                    </div>
                </li>
                <li class="bold waves-effect waves-red"><a class="collapsible-header white-text bold-text">Setting<i class="material-icons chevron white-text">chevron_left</i></a>
                    <div class="collapsible-body">
                        <ul>
                            <li><a href="#" class="waves-effect waves-red white-text bold-text"><i class="material-icons white-text">show_chart</i></a></li>
                            <li><a href="#" class="waves-effect waves-red white-text bold-text">Setting<i class="material-icons white-text">settings</i></a></li>
                        </ul>
                    </div>
                </li>
            </ul>
        </li>
    </ul>

</header>