@extends('master')
@section('content')

    <main class="card pdd-20px ">
        <div class="row">
            <div class="col s12 m12 l12 xl12 pdd-30px">
                <h1 class="title fontMoul"> ពាក្យសុំអនុញ្ញាតច្បាប់ឈប់សម្រាក </h1>
                <h1 class="title"> Leave Application Form </h1>
            </div>
        </div>
        <div class="row border">

            <div class="col s12 m6 l6 xl3">
                <label>
                    <input class="with-gap" name="group1" type="radio" />
                    <span class="fontContent text">ឈប់សម្រាកប្រចាំឆ្នាំ</span>
                </label>
            </div>

            <div class="col s12 m6 l6 xl3">
                <label>
                    <input class="with-gap" name="group2" type="radio" />
                    <span class="fontContent text">ឈប់សម្រាកមានជំងឺ</span>
                </label>
            </div>

            <div class="col s12 m6 l6 xl3">
                <label>
                    <input class="with-gap" name="group3" type="radio" />
                    <span class="fontContent text">ឈប់សម្រាកលំហែមាតុភាព</span>
                </label>
            </div>

            <div class="col s12 m6 l6 xl3">
                <label>
                    <input class="with-gap" name="group4" type="radio" />
                    <span class="fontContent text">ឈប់សម្រាកពិសេស</span>
                </label>
            </div>

            <div class="col s12 m6 l6 xl3">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" class="datepicker" id="fromdate-1" required>
                        <label for="fromdate-1" class="fontContent">ចាប់ពីថ្ងៃទី <span class="red-text">*</span></label>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6 xl3">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" class="datepicker" id="todate-1" required>
                        <label for="todate-1" class="fontContent">ដល់ថ្ងៃទី <span class="red-text">*</span></label>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6 xl3">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" class="timepicker" id="fromtime-1">
                        <label for="fromtime-1" class="fontContent">ចាប់ពីម៉ោង <span class="red-text">*</span></label>
                    </div>
                </div>
            </div>
            <div class="col s12 m6 l6 xl3">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" class="timepicker" id="totime-1">
                        <label for="totime-1" class="fontContent">រហូតដល់ម៉ោង <span class="red-text">*</span></label>
                    </div>
                </div>
            </div>

            <div class="col s12 m6 l6 xl6">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="dl1" class="autocomplete" required>
                        <label for="dl1" class="fontContent">ចំនួនថ្ងៃឈប់សម្រាក <span class="red-text">*</span></label>
                    </div>
                </div>
            </div>

            <div class="col s12 m6 l6 xl6">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="tl1" class="autocomplete" required>
                        <label for="tl1" class="fontContent">ចំនូនម៉ោងឈប់សម្រាក <span class="red-text">*</span></label>
                    </div>
                </div>
            </div>

            <div class="col s12 m6 l6 xl6">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="hb1" class="autocomplete" required>
                        <label for="hb1" class="fontContent">ខ្ញុំបាទ/នាងសូមផ្ទេការងារជូនឈ្មោះ</label>
                    </div>
                </div>
            </div>

            <div class="col s12 m6 l6 xl6">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="p1" class="autocomplete" required>
                        <label for="p1" class="fontContent">តួនាទី</label>
                    </div>
                </div>
            </div>

            <div class="col s12 m12 l12 xl12">
                <div class="row">
                    <div class="input-field col s12">
                        <textarea id="textarea1" class="materialize-textarea"></textarea>
                        <label for="textarea1" class="fontContent">មូលហេតុនៃការឈប់សម្រាក៖ <span class="red-text">*</span></label>
                    </div>
                </div>
            </div>

        </div>
    </main>

@endsection
